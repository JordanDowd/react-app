import React, {useEffect, useState} from 'react';
import Recipe from './Recipe';
import './App.css';

require('dotenv/config');

const App = () => {

  //const APP_ID = "";
  //const APP_KEY = "";

  //Recipes are sorted as an array of objects
  const[recipes, setRecipes] = useState([]);  
  const[search, setSearch] = useState("");
  const[query, setQuery] = useState('chicken');

  //Every time the page gets rendered, useEffect runs
  //We can add a second parameter that runs this once or for a cetain action
  //Having [] runs this only once when the page renders 
  //When query is changed, useEffect will run. Only changes when form submits
  useEffect(() => {
    const getAPIResponse = async () => {
      const response = await fetch(`https://api.edamam.com/search?q=${query}&app_id=${process.env.APP_ID}&app_key=${process.env.APP_KEY}`);
      const data = await response.json();
      setRecipes(data.hits);
      /*console.log(data);*/
    };

    getAPIResponse();
  }, [query]);  

  const updateSearch = e => {
    //Gets the input from the user
    setSearch(e.target.value);
  };

  const getSearch = e => {
    //Prevent page refresh on form submit
    //Sets the query to the users final input
    //Returns input to empty
    e.preventDefault();
    setQuery(search);
    setSearch('');
  };

  return (
    <div className="App">
      <form onSubmit={getSearch} className="search-form">
        <input className="search-bar" type="text" value={search} onChange={updateSearch}/>
        <button className="search-button" type="submit">Search</button>
      </form>
      <div className="recipes">
        {recipes.map(recipe => (
          /*Map the recipe information to Recipe.js
            () are added so we can return html */
          <Recipe 
            key={recipe.recipe.label}
            title={recipe.recipe.label} 
            ingredients={recipe.recipe.ingredients}
            calories={recipe.recipe.calories}
            image={recipe.recipe.image}/>
        ))}
      </div>
    </div>
  );
};

export default App;