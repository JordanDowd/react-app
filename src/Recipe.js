import React from 'react'
import style from "./recipe.module.css"

const Recipe = ({title, calories, image, ingredients}) => {
    return(
        <div className={style.recipe}>  
            <div className={style.image}>
                <img src={image} alt=""/>            
            </div>          
            <div className={style.card}>
            <h2>{title}</h2>
            <p>calories: {calories}</p>
            <p>ingredients:</p>
            <ol>
                {ingredients.map(ingredient => (
                    <li>{ingredient.text}</li>
                ))}
            </ol>
            </div>
        </div>
    );
}

export default Recipe;